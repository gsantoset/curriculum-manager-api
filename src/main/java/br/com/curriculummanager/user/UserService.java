package br.com.curriculummanager.user;

import br.com.curriculummanager.core.service.CrudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UserService extends CrudService<User, Long> {

    @Autowired private UserRepository repository;
    @Override public UserRepository getRepository() {return repository;}
}
