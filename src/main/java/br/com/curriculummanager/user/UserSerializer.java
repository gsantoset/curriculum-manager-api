package br.com.curriculummanager.user;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class UserSerializer extends JsonSerializer<User> {

	@Override
	public void serialize(User user, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		gen.writeStartObject();
		gen.writeNumberField("id", user.getId());
		gen.writeStringField("name", user.getName());
		gen.writeStringField("email", user.getEmail());
		gen.writeStringField("username", user.getUsername());
		gen.writeObjectField("graduations", user.getGraduations());
		gen.writeObjectField("experiences", user.getExperiences());
		gen.writeObjectField("languages", user.getLanguages());
		gen.writeObjectField("courses", user.getCourses());
		gen.writeObjectField("projects", user.getProjects());
		gen.writeEndObject();
	}
	
	

}
