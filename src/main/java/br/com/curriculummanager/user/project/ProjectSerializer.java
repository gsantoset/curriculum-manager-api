package br.com.curriculummanager.user.project;

import br.com.curriculummanager.user.project.Project;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

public class ProjectSerializer extends JsonSerializer<Set<Project>> {
    @Override
    public void serialize(Set<Project> projects, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartArray();
        for (Project project : projects){
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", project.getId());
            jsonGenerator.writeStringField("name", project.getName());
            jsonGenerator.writeStringField("description", project.getDescription());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
