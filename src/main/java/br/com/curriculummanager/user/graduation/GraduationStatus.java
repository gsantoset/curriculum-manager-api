package br.com.curriculummanager.user.graduation;

public enum GraduationStatus {
    COMPLETED, ON_GOING, DROPPED
}
