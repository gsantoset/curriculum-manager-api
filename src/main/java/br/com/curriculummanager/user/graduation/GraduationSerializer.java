package br.com.curriculummanager.user.graduation;

import br.com.curriculummanager.user.graduation.Graduation;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

public class GraduationSerializer extends JsonSerializer<Set<Graduation>> {

    @Override
    public void serialize(Set<Graduation> graduations, JsonGenerator gen, SerializerProvider serializerProvider) throws IOException {
        gen.writeStartArray();
        for (Graduation graduation : graduations){
            gen.writeStartObject();
            gen.writeNumberField("id", graduation.getId());
            gen.writeStringField("college", graduation.getCollege());
            gen.writeStringField("course", graduation.getCourse());
            gen.writeStringField("start", graduation.getStart().toString());
            gen.writeStringField("end", graduation.getEnd().toString());
            gen.writeEndObject();
        }
        gen.writeEndArray();

    }
}
