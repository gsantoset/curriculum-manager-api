package br.com.curriculummanager.user.course;

import br.com.curriculummanager.user.course.Course;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

public class CourseSerializer extends JsonSerializer<Set<Course>> {
    @Override
    public void serialize(Set<Course> courses, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartArray();
        for (Course course : courses) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", course.getId());
            jsonGenerator.writeStringField("title", course.getTitle());
            jsonGenerator.writeStringField("institution", course.getInstitution());
            jsonGenerator.writeNumberField("duration", course.getDuration());
            jsonGenerator.writeStringField("metric", course.getMetric().name());
            jsonGenerator.writeStringField("end", course.getEnd().toString());
            jsonGenerator.writeStringField("certificateUrl", course.getCertificateUrl());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
