package br.com.curriculummanager.user.course;

public enum CourseDurationMetric {
    MINUTE, HOUR
}
