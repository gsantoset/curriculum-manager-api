package br.com.curriculummanager.user;

import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserResource {
    private final UserService service;
    private final ModelMapper mapper;

    public UserResource(UserService service, ModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping({"/", ""})
    public ResponseEntity<List<User>> getAll(){
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping({"", "/"})
    public ResponseEntity<User> create(@RequestBody UserRegisterDTO dto){
        return ResponseEntity.status(HttpStatus.CREATED).body(service.create(mapper.map(dto, User.class)));
    }



}
