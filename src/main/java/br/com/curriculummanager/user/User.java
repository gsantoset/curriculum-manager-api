package br.com.curriculummanager.user;

import br.com.curriculummanager.core.representation.AbstractRepresentation;
import br.com.curriculummanager.user.course.Course;
import br.com.curriculummanager.user.course.CourseSerializer;
import br.com.curriculummanager.user.experience.Experience;
import br.com.curriculummanager.user.experience.ExperienceSerializer;
import br.com.curriculummanager.user.graduation.Graduation;
import br.com.curriculummanager.user.graduation.GraduationSerializer;
import br.com.curriculummanager.user.language.Language;
import br.com.curriculummanager.user.language.LanguagesSerializer;
import br.com.curriculummanager.user.project.Project;
import br.com.curriculummanager.user.project.ProjectSerializer;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import java.util.Set;

@Entity
@Table(name = "user_")
public class User extends AbstractRepresentation<Long> {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name", nullable = false)
	private String name;
	
	@Column(name = "username", nullable = false, unique = true, updatable = false)
	private String username;
	
	@Email
	@Column(name = "email", nullable = false, unique = true)
	private String email;
	
	@Column(name = "password", nullable = false)
	@JsonProperty(access = Access.WRITE_ONLY)
	private String password;

	@OneToMany(mappedBy = "user", orphanRemoval = true)
	@JsonSerialize(using = GraduationSerializer.class)
	private Set<Graduation> graduations;

	@OneToMany(mappedBy = "user", orphanRemoval = true)
	@JsonSerialize(using = ExperienceSerializer.class)
	private Set<Experience> experiences;

	@OneToMany(mappedBy = "user", orphanRemoval = true)
	@JsonSerialize(using = LanguagesSerializer.class)
	private Set<Language> languages;

	@OneToMany(mappedBy = "user", orphanRemoval = true)
	@JsonSerialize(using = CourseSerializer.class)
	private Set<Course> courses;

	@OneToMany(mappedBy = "user", orphanRemoval = true)
	@JsonSerialize(using = ProjectSerializer.class)
	private Set<Project> projects;

	@Column(name = "active", columnDefinition = "BOOLEAN DEFAULT TRUE")
	private Boolean active = true;

	@Override
	public Long getId() { return id; }

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Graduation> getGraduations() {
		return graduations;
	}

	public void setGraduations(Set<Graduation> graduations) {
		this.graduations = graduations;
	}

	public Set<Experience> getExperiences() {
		return experiences;
	}

	public void setExperiences(Set<Experience> experiences) {
		this.experiences = experiences;
	}

	public Set<Language> getLanguages() {
		return languages;
	}

	public void setLanguages(Set<Language> languages) {
		this.languages = languages;
	}

	public Set<Course> getCourses() {
		return courses;
	}

	public void setCourses(Set<Course> courses) {
		this.courses = courses;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

	@Override
	public Boolean getActive() {
		return active;
	}

	@Override
	public void setActive(Boolean active) {
		this.active = active;
	}
}
