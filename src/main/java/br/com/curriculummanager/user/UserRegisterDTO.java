package br.com.curriculummanager.user;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserRegisterDTO {

    @NotBlank
    private String name;

    @NotBlank
    private String username;

    @NotBlank
    private String email;

    @NotBlank
    private String password;
}
