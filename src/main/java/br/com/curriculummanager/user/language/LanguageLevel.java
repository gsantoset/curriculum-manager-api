package br.com.curriculummanager.user.language;

public enum LanguageLevel {
    BASIC, INTERMEDIATE, UPPER_INTERMEDIATE, ADVANCED, FLUENT
}
