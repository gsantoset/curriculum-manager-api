package br.com.curriculummanager.user.language;

import br.com.curriculummanager.user.language.Language;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

public class LanguagesSerializer extends JsonSerializer<Set<Language>> {
    @Override
    public void serialize(Set<Language> languages, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartArray();
        for (Language language : languages){
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", language.getId());
            jsonGenerator.writeStringField("language", language.getLanguage());
            jsonGenerator.writeStringField("level", language.getLevel().name());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
