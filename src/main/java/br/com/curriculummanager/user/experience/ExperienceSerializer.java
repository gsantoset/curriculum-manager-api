package br.com.curriculummanager.user.experience;

import br.com.curriculummanager.user.experience.Experience;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Set;

public class ExperienceSerializer extends JsonSerializer<Set<Experience>> {

    @Override
    public void serialize(Set<Experience> experiences, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartArray();
        for (Experience experience : experiences){
            jsonGenerator.writeStartObject();
            jsonGenerator.writeNumberField("id", experience.getId());
            jsonGenerator.writeStringField("company", experience.getCompany());
            jsonGenerator.writeStringField("position", experience.getPosition());
            jsonGenerator.writeStartArray();
            String[] responsibilities = experience.getResponsibility().split(";;");
            jsonGenerator.writeArray(responsibilities, 0, responsibilities.length);
            jsonGenerator.writeEndArray();
            jsonGenerator.writeStringField("start", experience.getStart().toString());
            jsonGenerator.writeStringField("end", experience.getEnd().toString());
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
    }
}
