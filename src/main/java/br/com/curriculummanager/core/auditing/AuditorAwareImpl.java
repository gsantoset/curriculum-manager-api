package br.com.curriculummanager.core.auditing;

import br.com.curriculummanager.user.User;
import br.com.curriculummanager.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<User> {

    @Autowired private UserRepository repository;

    @Override
    public Optional<User> getCurrentAuditor() {
        return repository.findByUsername("gustavo.santos");
    }
}
