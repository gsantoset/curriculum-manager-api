package br.com.curriculummanager.core.representation;

import java.io.Serializable;

public abstract class AbstractRepresentation<T extends Serializable> implements Representation<T> {
	public AbstractRepresentation() {}
	public AbstractRepresentation(T id) {setId(id);}
}
