package br.com.curriculummanager.core.representation;

import java.io.Serializable;

public interface Representation<T extends Serializable> {
	public T getId();
	public void setId(T id);
	public Boolean getActive();
	public void setActive(Boolean b);
}
