package br.com.curriculummanager.core.service;

import br.com.curriculummanager.core.representation.Representation;

import java.io.Serializable;

public abstract class CrudService <R extends Representation<I>, I extends Serializable> extends BaseService<R, I> {

    public R create(R representation){
        return getRepository().save(representation);
    }

    public <S extends R> S createSubType (S representation){
        @SuppressWarnings("unchecked")
        S subtypeRepresentation = (S) create(representation);
        return subtypeRepresentation;
    }
}
