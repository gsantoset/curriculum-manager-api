package br.com.curriculummanager.core.service;

import br.com.curriculummanager.core.repository.BaseRepository;
import br.com.curriculummanager.core.representation.Representation;
import org.springframework.core.GenericTypeResolver;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;

@Service
@Transactional
public abstract class BaseService<R extends Representation<I>, I extends Serializable> {
    public abstract BaseRepository<R, I> getRepository();
    private Class<R> representationClass;

    public Class<R> getRepresentationClass(){
        if (representationClass == null)
            representationClass = (Class<R>) GenericTypeResolver.resolveTypeArguments(getClass(), BaseService.class)[0];
        return representationClass;
    }

    public List<R> getAll(){
        return getRepository().findAll();
    }
}
